using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class invertory : MonoBehaviour
{
    public GameObject ok1;
    public GameObject ok2;
    public GameObject ok3;

    public GameObject weapon1;
    public GameObject weapon2;
    public GameObject weapon3;


    void Update()
    {
        if (Input.GetKeyDown("1"))
        {
            ok1.SetActive(true);
            ok2.SetActive(false);
            ok3.SetActive(false);
        }

        if (Input.GetKeyDown("2"))
        {
            ok2.SetActive(true);
            ok1.SetActive(false);
            ok3.SetActive(false);
        }

        if (Input.GetKeyDown("3"))
        {
            ok3.SetActive(true);
            ok1.SetActive(false);
            ok2.SetActive(false);
        }




        if (Input.GetKey(KeyCode.Z))
        {
            weapon1.GetComponent<Image>().material = null;
        }

        if (Input.GetKey(KeyCode.X))
        {
            weapon2.GetComponent<Image>().material = null;
        }

        if (Input.GetKey(KeyCode.C))
        {
            weapon3.GetComponent<Image>().material = null;
        }
    }
}
