using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotater : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Debug.Log("QRot");
            gameObject.transform.Rotate(0, 90, 0, Space.World);
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("ERot");
            gameObject.transform.Rotate(0, -90, 0, Space.World);
        }
    }
}
