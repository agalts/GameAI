using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    [SerializeField] float _movementDistance = 10f;

    int _currentCorner = 2;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            MoveCounterclockwise();
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            MoveClockwise();
        }
    }
    private void MoveClockwise()
    {
        if (_currentCorner == 3)
        {
            gameObject.transform.Translate(_movementDistance, 0, 0, Space.World);
        }
        else if (_currentCorner == 2)
        {
            gameObject.transform.Translate(0, 0, _movementDistance, Space.World);
        }
        else if (_currentCorner == 1)
        {
            gameObject.transform.Translate(-_movementDistance, 0, 0, Space.World);
        }
        else if (_currentCorner == 0)
        {
            gameObject.transform.Translate(0, 0, -_movementDistance, Space.World);
        }

        _currentCorner++;

        if (_currentCorner == -1)
            _currentCorner = 3;
    }
    private void MoveCounterclockwise()
    {
        if(_currentCorner == 0)
        {
            gameObject.transform.Translate(_movementDistance, 0, 0, Space.World);
        }
        else if(_currentCorner == 1)
        {
            gameObject.transform.Translate(0, 0, -_movementDistance, Space.World);
        }
        else if (_currentCorner == 2)
        {
            gameObject.transform.Translate(-_movementDistance, 0, 0, Space.World);
        }
        else if (_currentCorner == 3)
        {
            gameObject.transform.Translate(0, 0, _movementDistance, Space.World);
        }

        _currentCorner++;

        if (_currentCorner == 4)
            _currentCorner = 0;
    }
}
