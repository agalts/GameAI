using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerCondition
{
    Normal,
    Run,
    Sit
}

public class Movement : MonoBehaviour
{
    public GameObject MainCamera;
    public GameObject StayCamera;

    public float speed = 8;
    public float sitSpeed = 5;
    public float runSpeed = 11;
    public float soundRadius = 0;
    public float earth_lvl_Y = 1;

    public static bool _gameover;
    private bool grounded;

    public PlayerCondition condition;
    private Rigidbody rb;

    void Start()
    {
        StayCamera.transform.position = MainCamera.transform.position;
        StayCamera.transform.rotation = MainCamera.transform.rotation;
        StayCamera.transform.rotation.Set(0, StayCamera.transform.rotation.y, StayCamera.transform.rotation.z, StayCamera.transform.rotation.w);

        condition = PlayerCondition.Normal;
        grounded = true;
        rb = this.GetComponent<Rigidbody>();
        _gameover = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.E))
        {
            StayCamera.transform.position = MainCamera.transform.position;
            StayCamera.transform.rotation = MainCamera.transform.rotation;
            StayCamera.transform.rotation.Set(0, StayCamera.transform.rotation.y, StayCamera.transform.rotation.z, StayCamera.transform.rotation.w);
        }
    }

    private void FixedUpdate()
    {

        if (Input.GetKey(KeyCode.LeftShift)) { condition = PlayerCondition.Run; soundRadius = 10; }
        else if (Input.GetKey(KeyCode.LeftControl)) { condition = PlayerCondition.Sit; soundRadius = 2; }
        else { condition = PlayerCondition.Normal; soundRadius = 5; }

        float mySpeed;
        if (condition == PlayerCondition.Sit) { mySpeed = sitSpeed; }
        else if (condition == PlayerCondition.Run) { mySpeed = runSpeed; }
        else { mySpeed = speed; }

        if (Input.GetKey(KeyCode.W) & _gameover == false)
        {
            Vector3 posW = rb.transform.position + StayCamera.transform.forward * mySpeed * Time.deltaTime; posW.y = earth_lvl_Y;
            if (grounded == true) { rb.MovePosition(posW); }
        }

        if (Input.GetKey(KeyCode.S) & _gameover == false)
        {
            Vector3 posS = rb.transform.position + (-StayCamera.transform.forward * mySpeed * Time.deltaTime); posS.y = earth_lvl_Y;
            if (grounded == true) { rb.MovePosition(posS); }
        }

        if (Input.GetKey(KeyCode.D) & _gameover == false)
        {
            if (grounded == true) { rb.MovePosition(rb.transform.position + StayCamera.transform.right * mySpeed * Time.deltaTime); }
        }
        if (Input.GetKey(KeyCode.A) & _gameover == false)
        {
            if (grounded == true) { rb.MovePosition(rb.transform.position + (-StayCamera.transform.right * mySpeed * Time.deltaTime)); }
        }
    }


}
