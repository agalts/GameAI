using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Hearing : MonoBehaviour
{
    public float maxRange = 12.0f; 
    public string SoundTag = "Sound";
    private GameObject[] SoundObjects;

    void Update()
    {
        SoundObjects = GameObject.FindGameObjectsWithTag(SoundTag);
        foreach (GameObject sound in SoundObjects)
        {
            if (sound.GetComponent<AudioSource>().isPlaying)
            {
                Vector3 offset = sound.transform.position - transform.position;
                if (offset.sqrMagnitude < maxRange * maxRange) 
                {
                    Debug.Log("УСЛЫШАЛ ЗВУК!");
                    //sound.IsEnabled = false;
                }
            }
        }
    }
}
