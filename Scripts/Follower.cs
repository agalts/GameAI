using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{
    [SerializeField] GameObject _object;
    private Vector3 _lastPosition;

    private void Start()
    {
        _lastPosition = _object.transform.position;
    }

    private void Update()
    {
        Vector3 deltaPos = _object.transform.position - _lastPosition;
        if(deltaPos != new Vector3())
        {
            gameObject.transform.position += deltaPos;
        }
        _lastPosition = _object.transform.position;
    }
}
